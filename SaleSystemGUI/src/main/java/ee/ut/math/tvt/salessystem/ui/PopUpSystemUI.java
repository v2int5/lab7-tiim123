package ee.ut.math.tvt.salessystem.ui;

import java.net.URL;
import java.util.ResourceBundle;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;

import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class PopUpSystemUI extends Stage implements Initializable{

private final SalesSystemDAO dao;
private final ShoppingCart shoppingCart;  //instead of list of sold items use a shopping cart

@FXML 
private Label orderSum;
@FXML 
private Button confirmPurchase;
@FXML 
private Button cancelPurchase;
@FXML 
private TextField setSum;
@FXML 
private Label returnMoney;

private boolean saleCompleted;

public PopUpSystemUI(Parent parent, SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PopUpSystemUI.fxml")); //create an fxml file for this window
        fxmlLoader.setController(this);
        super.setTitle("Confirmation Dialogue");
        try{
            setScene(new Scene((Parent) fxmlLoader.load()));
        }catch(IOException e){
            e.printStackTrace();
        }
        orderSum.setText(String.valueOf(shoppingCart.getPurchaseSum()));
        this.saleCompleted = false;
	}



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		this.setSum.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!newPropertyValue){
                	if(setSum.getText() != ""){
                		returnMoney.setText(String.valueOf(Double.valueOf(setSum.getText()) - Double.valueOf(orderSum.getText())));
                	}
                }
            }
        });
		
		
	}
    @FXML
    protected void cancelPurchaseButtonClicked() {
    	
        close();
           
     }

    @FXML
    protected void confirmPurchaseButtonClicked(){
    	if(Double.valueOf(setSum.getText()) >= Double.valueOf(orderSum.getText())){
	    	this.saleCompleted = true;
	    	close();
    	}else{
    		Alert alert = new Alert(AlertType.WARNING);
    		alert.setTitle("Warning!");
    		alert.setHeaderText("Total is more than cash given");
    		alert.setContentText("Client is " + String.valueOf(Double.valueOf(orderSum.getText()) - Double.valueOf(setSum.getText())) + " money short");

    		alert.showAndWait();
    	}
    }
    
    public boolean isSaleCompleted(){ 
    	return saleCompleted;
    }
}