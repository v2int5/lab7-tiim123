package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ee.ut.math.tvt.salessystem.ui.PopUpSystemUI;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ComboBox dropmenu;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	List<StockItem> list = dao.findStockItems();
    	ObservableList<String> options = FXCollections.observableArrayList();
    	for(int i = 0; i < list.size(); i++){
    		options.add(list.get(i).getName());
    	}
        dropmenu.setItems(options);
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(new ObservableListWrapper<>(shoppingCart.getAll()));
        disableProductField(true);

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem(true);
                }
            }
        });
        this.dropmenu.focusedProperty().addListener(new ChangeListener<Boolean>(){
        	@Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem(false);
                }
            }
        });
    }

    /** Event handler for the <code>new purchase</code> event. */
    
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
    	//shoppingCart.getAll().get(0);
            if (!shoppingCart.getAll().isEmpty()) {
                PopUpSystemUI popup = new PopUpSystemUI(null, dao, shoppingCart);
                popup.showAndWait();
                if (popup.isSaleCompleted()) { //check if accept cashier pressed accept or cancel button;
                    log.info("Sale complete");
                    try {
                        log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
                        shoppingCart.submitCurrentPurchase();
                        disableInputs();
                        purchaseTableView.refresh();
                    } catch (SalesSystemException e) {
                        log.error(e.getMessage(), e);
                        }
                    }
                }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem(boolean barcode) {
    	StockItem stockItem;
    	if(barcode){
    		stockItem = getStockItemByBarcode();
    	}else{
    		stockItem = getStockItemByName();
    	}
        
        
        if (stockItem != null) {
        	dropmenu.setValue(stockItem.getName());
            barCodeField.setText(String.valueOf(stockItem.getId()));
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByName(){  	
   		String item = String.valueOf(dropmenu.getValue());
   		return dao.findStockItemByName(item);
    	//long code = Long.parselong(arg0)
    	
    }
    
    
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
        	//log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
    	StockItem stockItem;
        if (Integer.valueOf(quantityField.getText()) < 0 || Integer.valueOf(priceField.getText()) < 0){
            log.warn("Negative value in quantity or price.");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Error negative value");
            alert.setContentText("Quantity or total price are wrong");
            alert.showAndWait();
        } else {
            if (barCodeField.getText() == "") {
                stockItem = getStockItemByName();
            } else {
                // add chosen item to the shopping cart.
                stockItem = getStockItemByBarcode();
            }
            if (stockItem != null) {
                int quantity;
                try {
                    quantity = Integer.parseInt(quantityField.getText());
                } catch (NumberFormatException e) {
                    log.error(e.getMessage(), e);
                    quantity = 1;
                }
                shoppingCart.addItem(new SoldItem(stockItem, quantity));
                purchaseTableView.refresh();
            }
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.dropmenu.setDisable(disable);
        //this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        dropmenu.setValue("");
        //nameField.setText("");
        priceField.setText("");
    }
}
