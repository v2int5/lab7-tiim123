
package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.javafx.collections.ObservableListWrapper;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;

/**
* Encapsulates everything that has to do with the purchase tab (the tab
* labelled "History" in the menu).
*/
public class HistoryController implements Initializable {
    
   private final SalesSystemDAO dao;
   private final HistoryLogic historyLogic;
   private static final Logger log = LogManager.getLogger(HistoryController.class);
    @FXML
    private Button dateButton;
    @FXML
    private Button last10Button;
    @FXML
    private Button allButton;
    
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
   private TableView<HistoryItem> historyTableView;
    @FXML
    private TableView<SoldItem> soldTableView;
    
    
   public HistoryController(SalesSystemDAO dao, HistoryLogic historyLogic) {
       this.dao = dao;
       this.historyLogic = historyLogic;
   }
    
   @Override
   public void initialize(URL location, ResourceBundle resources) {
       historyTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
           if (newSelection != null) {
               //soldTableView.getItems().clear();
               HistoryItem selected = historyTableView.getSelectionModel().getSelectedItem();
               //log.info(String.valueOf(selected.getSoldItems().size()));
               soldTableView.setItems(new ObservableListWrapper<>(selected.getSoldItems()));
               soldTableView.refresh();
               log.info("HistoryTableView clicked");
           }
       });
       
   }
   
   
   @FXML
   protected void allButtonClicked(){
       log.info("allButton clicked");
       historyTableView.setItems(new ObservableListWrapper<>(dao.findAllHistoryItems()));
       historyTableView.refresh();
       
       
       
   }
   
   @FXML
   protected void last10ButtonClicked(){
	   log.info("last10Button clicked");
	   historyTableView.setItems(new ObservableListWrapper<>(historyLogic.find10Last()));
	   historyTableView.refresh();
   }
   
   @FXML
   protected void dateButtonClicked(){
	   log.info("dateButton clicked");
	   historyTableView.setItems(new ObservableListWrapper<>(historyLogic.findByDate(startDate.getValue(), endDate.getValue())));
	   historyTableView.refresh();
	   
	   
	   
	   
	   
   }
}