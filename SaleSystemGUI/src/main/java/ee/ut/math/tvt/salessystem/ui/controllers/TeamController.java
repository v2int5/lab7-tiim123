package ee.ut.math.tvt.salessystem.ui.controllers;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class TeamController implements Initializable {
	@FXML
	ImageView imageView;
	@FXML
	Label teamleader;
	@FXML
	Label emailadr;
	@FXML
	Label members;

	
	//private static final Logger log = LogManager.getLogger(TeamController.class);
	public TeamController(){
    	
    }
    @Override
    
    public void initialize(URL location, ResourceBundle resources) {
   
    	Properties prop = new Properties();
		InputStream input = null;
		try{
			input = getClass().getClassLoader().getResourceAsStream("application.properties");
			prop.load(input);
		}catch (IOException io){
			io.printStackTrace();
		}finally {
			if (input != null){
				teamleader.setText(prop.getProperty("teamleader"));
				emailadr.setText(prop.getProperty("emailadr"));
				members.setText(prop.getProperty("members"));
				Image image2 = new Image(prop.getProperty("logo"));
				imageView.setImage(image2);
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
    }
}
