package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private static final Logger log = LogManager.getLogger(PurchaseController.class);
    private final WarehouseLogic logic;

    @FXML
	private TextField barcode;
	@FXML
	private TextField name;
	@FXML
	private TextField quantity;
	@FXML
	private TextField price;

    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
        this.logic = new WarehouseLogic(dao);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        // TODO refresh view after adding new items
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }
    @FXML
    protected void cancelButtonClicked(){
    	barcode.setText("");
    	name.setText("");
    	price.setText("");
    	quantity.setText("");
    	log.info("Product adding cancelled.");
    }

    @FXML
    protected void acceptButtonClicked(){
        if (barcode.getText().equals("") || name.getText().equals("") || price.getText().equals("") || quantity.getText().equals("")) {
            log.warn("Tried adding items to warehouse with empty fields.");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Empty fields");
            alert.setContentText("You have forgotten to fill some of the blank fields.");
            alert.showAndWait();
        } else if (dao.findStockItemByName(name.getText()).getId() != null) {
            log.warn("Tried adding an existing product to the warehouse");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText("Error this item already exists");
            alert.setContentText("Please user another name.");
            alert.showAndWait();
        } else {
            try {
                log.debug("Contents of the current warehouse:\n" + warehouseTableView.getColumns());
                Long id = Long.valueOf(barcode.getText());
                logic.addItem(id, Integer.valueOf(quantity.getText()), name.getText(), Double.valueOf(price.getText()));
                log.info("Adding new item(s) to stock completed.");
                warehouseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }

        }
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findStockItems()));
        warehouseTableView.refresh();
        log.info("Stock refresh completed.");
    }


}
