package ee.ut.math.tvt.salessystem.dataobjects;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SoldItem.class)
public abstract class SoldItem_ {

	public static volatile SingularAttribute<SoldItem, HistoryItem> historyItem;
	public static volatile SingularAttribute<SoldItem, Integer> quantity;
	public static volatile SingularAttribute<SoldItem, Double> price;
	public static volatile SingularAttribute<SoldItem, String> name;
	public static volatile SingularAttribute<SoldItem, StockItem> stockItem;
	public static volatile SingularAttribute<SoldItem, Double> finalPrice;
	public static volatile SingularAttribute<SoldItem, Long> id;

}

