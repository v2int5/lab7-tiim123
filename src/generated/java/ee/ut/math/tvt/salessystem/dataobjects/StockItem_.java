package ee.ut.math.tvt.salessystem.dataobjects;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StockItem.class)
public abstract class StockItem_ {

	public static volatile SingularAttribute<StockItem, Integer> quantity;
	public static volatile SingularAttribute<StockItem, Double> price;
	public static volatile SingularAttribute<StockItem, String> name;
	public static volatile SingularAttribute<StockItem, String> description;
	public static volatile SingularAttribute<StockItem, Long> id;

}

