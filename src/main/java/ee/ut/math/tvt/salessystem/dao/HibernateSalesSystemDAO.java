package ee.ut.math.tvt.salessystem.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

	@Override
	public List<StockItem> findStockItems() {
		return em.createQuery("from StockItem", StockItem.class).getResultList();
	}

	@Override
	public StockItem findStockItem(long id) {
		return em.find(StockItem.class, id);
        //return em.createQuery("from STOCK SELECT * WHERE id = " + id, StockItem.class).getSingleResult();
	}

	@Override
	public StockItem findStockItemByName(String name) {
        //return em.find(StockItem.class, name);
        try {
           return em.createQuery("SELECT i FROM StockItem i WHERE i.name LIKE :iName", StockItem.class).setParameter("iName",name).getSingleResult();
        } catch(Exception InvocationTargetException) {
            return new StockItem();
        }
	}
    /*
	@Override
    public StockItem findStockItemByName(final String name) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> criteria = builder.createQuery(StockItem.class);
        Root<StockItem> from = criteria.from(StockItem.class);
        criteria.select(from);
        criteria.where(builder.equals(from.get(StockItem_.name), name));
        TypedQuery<StockItem> typed = em.createQuery(criteria);
        try {
            return typed.getSingleResult();
        } catch (final NoResultException nre) {
            return null;
        }
    }*/

	@Override
	public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
	}

	@Override
	public void saveSoldItem(SoldItem item) {
        em.persist(item);
	}

	@Override
	public List<HistoryItem> findAllHistoryItems() {
		return em.createQuery("From HistoryItem", HistoryItem.class).getResultList();
	}
}