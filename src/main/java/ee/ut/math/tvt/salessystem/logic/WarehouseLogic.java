package ee.ut.math.tvt.salessystem.logic;

import java.util.List;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;


public class WarehouseLogic {
	private final SalesSystemDAO dao;
	private final List<StockItem> items;
	
	public WarehouseLogic(SalesSystemDAO dao){
		this.dao = dao;
		this.items = dao.findStockItems();
	}
	
	public void addItem(long id, int quantity, String name, double price){
	
		if (dao.findStockItem(id) != null) {
            dao.findStockItem(id).setQuantity(dao.findStockItem(id).getQuantity() + quantity);
        } else {
            dao.saveStockItem(new StockItem(id, name, "", price, quantity));
        }
	}
}
