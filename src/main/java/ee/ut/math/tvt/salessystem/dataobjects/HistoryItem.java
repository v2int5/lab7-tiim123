package ee.ut.math.tvt.salessystem.dataobjects;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "HistoryItem")
public class HistoryItem {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @OneToMany(mappedBy = "historyItem")    
	private List<SoldItem> soldItems;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "time")
	private String time;//TODO bad practice
	
	@Column(name = "total")
	private double total;
	
	@Column(name = "datetime")
	private LocalDateTime dateTime;
	
	
	public HistoryItem(Long id, double total, List<SoldItem> items) {
		this.id = id;
		this.soldItems = items;
        this.dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        this.date = dateTime.format(formatter);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        this.time = dateTime.format(formatter2);
        this.total = total; 
	}

	public Long getId() {
		return id;
	}
	public List<SoldItem> getSoldItems() {
		return soldItems;
	}
	public String getDate() {
		return date;
	}
	public String getTime() {
		return time;
	}
	public double getTotal() {
		return total;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	
	@Override
	public String toString() {
		return "HistoryItem [date=" + date + ", time=" + time + ", total=" + total + "]";
	}
	
	
	
	
	
	
	
}
