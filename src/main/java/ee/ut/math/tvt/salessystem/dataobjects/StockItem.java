package ee.ut.math.tvt.salessystem.dataobjects;

import java.util.Set;

import javax.persistence.*;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;


/**
 * Stock item.
 */

@Entity
@Table(name = "StockItem")
public class StockItem {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "name", unique = true, nullable = false)
    private String name;
    
    @Column(name = "price")
    private double price;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "quantity")
    private int quantity;

    @Transient//EITOHIKSSIIN OLLA
    private int quantityDuringShopping; // to check if we are running out of items before checkout.

   
    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
        this.quantityDuringShopping = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        this.quantityDuringShopping = quantity;
    }
    
    public void setQuantityToQDS(){
    	this.quantity = this.quantityDuringShopping;
    }

    public int getQuantityDuringShopping() {
		return quantityDuringShopping;
	}

	public void decreaseQuantityDuringShopping(int quantity) { // 
		this.quantityDuringShopping -= quantity;
	}
	
	public void resetQuantityDuringShopping(){
		this.quantityDuringShopping = quantity;
	}

	@Override
    public String toString() {
        return String.format("StockItem{id=%d, name='%s'}", id, name);
    }
}
