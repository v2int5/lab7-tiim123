package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

	private static final Logger log = LogManager.getLogger(InMemorySalesSystemDAO.class);
    private List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<HistoryItem> historyItemList;
    private HistoryItem historyItem;
    private List<String> transactions;
	private List<StockItem> inMemoryStockItemList;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.historyItemList = new ArrayList<>();
        transactions = new ArrayList<String>();
        inMemoryStockItemList = new ArrayList<StockItem>(stockItemList);
        
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }
    
    public StockItem findStockItemByName(String name){
    	for(StockItem item : stockItemList){
    		if(item.getName().equals(name)){
    			return item;
    		}
    	}
    	return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void beginTransaction() {
    	transactions.add("begin");
    }

    @Override
    public void rollbackTransaction() {
    	historyItem = null;
    }
    

    @Override
    public void commitTransaction() {
    	transactions.add("commit");
    	double total = 0;
        
    	for(SoldItem item : soldItemList){
    		total += item.getFinalPrice();
    	}
    	historyItemList.add(new HistoryItem(Long.valueOf(historyItemList.size())
    			, total, new ArrayList<SoldItem>(soldItemList)));
    	soldItemList.clear();
    	
    	
    }

	@Override
	public List<HistoryItem> findAllHistoryItems() {
		return historyItemList;
	}

	public List<String> getTransactions() {
		return transactions;
	}

	public void resetTransactions() {
		this.transactions = new ArrayList<String>();
	}
	
	public void resetStockItems(){
		stockItemList = new ArrayList<StockItem>(inMemoryStockItemList);
	}
	
	public HistoryItem getLastHistoryItem(){
		return historyItemList.get(historyItemList.size()-1);
	}
	
	
}
