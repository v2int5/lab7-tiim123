package ee.ut.math.tvt.salessystem.logic;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;

public class HistoryLogic {
	private final SalesSystemDAO dao;
    private List<HistoryItem> items = new ArrayList<>();
    
    public HistoryLogic(SalesSystemDAO dao){
    	this.dao = dao;
    	//this.items = this.dao.findAllHistoryItems();
    }
    
    public List<HistoryItem> find10Last(){
    	List<HistoryItem> historyItems = new ArrayList<>();
    	int i = 0;
    	if(items.size() > 10){
    		i = items.size() - 11;
    	}
    	while(i < items.size()){
    		historyItems.add(items.get(i));
    		i++;
    	}
    	
    	return historyItems;
    }
    
    public List<HistoryItem> findByDate(LocalDate startDate, LocalDate endDate){
    	
 	   	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
 	   	String startDateString = startDate.format(formatter);
 	   	String endDateString = endDate.format(formatter);
    	List<HistoryItem> historyItems = new ArrayList<>();
    	startDateString += " 00:00";
    	endDateString += " 23:59";
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); //seems a bit spaghetti,  but at the moment seems the easiest way to compare dates
        LocalDateTime startDateTime = LocalDateTime.parse(startDateString, formatter2);
        LocalDateTime endDateTime = LocalDateTime.parse(endDateString, formatter2);
        for(HistoryItem item : items){
        	if(item.getDateTime().compareTo(startDateTime) >= 0){
        		if(item.getDateTime().compareTo(endDateTime) <= 0){
        			historyItems.add(item);
        		}
        	}
        }
    	return historyItems;
    }
}
