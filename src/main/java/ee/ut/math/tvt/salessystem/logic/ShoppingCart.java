package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
//import ee.ut.math.tvt.salessystem.ui.controllers.PurchaseController;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShoppingCart {
	
	private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
    	
    	if(item.getQuantity() <= item.getStockItem().getQuantityDuringShopping()){
        // TODO In case such stockItem already exists increase the quantity of the existing stock
        // TODO verify that warehouse items' quantity remains at least zero or throw an exception
    	boolean isInCart = false;
    	for(SoldItem listItem : items){
    		if(listItem.getId() == item.getId()){
    			isInCart = true;
    			listItem.setQuantity(listItem.getQuantity() + item.getQuantity());
    		}
    	}
    	if(!isInCart){
    		 items.add(item);
    	}
       
        item.getStockItem().decreaseQuantityDuringShopping(item.getQuantity());
        //log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    	}
    	else{
    		
    		Alert alert = new Alert(AlertType.WARNING);
    		alert.setTitle("Error!");
    		alert.setHeaderText("Not enough " + item.getName() + " in stock!");
    		if(item.getStockItem().getQuantityDuringShopping() > 0){
    			alert.setContentText("There is only " + item.getStockItem().getQuantity() + " " + item.getName() + " items in stock left");
    		}else{
    			alert.setContentText(item.getName() + " has ran out of stock");
    		}
    		

    		alert.showAndWait();
    		//throw new SalesSystemException();
    		}
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
    	for(SoldItem item : items){
    		item.getStockItem().resetQuantityDuringShopping();
    	}
        items.clear();
    }

    public void submitCurrentPurchase() {
        for (SoldItem item : items) {
        	item.getStockItem().setQuantityToQDS();
            dao.saveSoldItem(item);
        }
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            log.error(e.getMessage(), e);
            throw e;
        }
    }
    public double getPurchaseSum(){
    	double sum = 0;
    	for(SoldItem item : items){
    		sum += item.getFinalPrice();
    	}
    	return sum;
    }
}
