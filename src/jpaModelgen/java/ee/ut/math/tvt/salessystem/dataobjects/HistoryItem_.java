package ee.ut.math.tvt.salessystem.dataobjects;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HistoryItem.class)
public abstract class HistoryItem_ {

	public static volatile SingularAttribute<HistoryItem, String> date;
	public static volatile SingularAttribute<HistoryItem, LocalDateTime> dateTime;
	public static volatile SingularAttribute<HistoryItem, Double> total;
	public static volatile SingularAttribute<HistoryItem, Long> id;
	public static volatile SingularAttribute<HistoryItem, String> time;
	public static volatile ListAttribute<HistoryItem, SoldItem> soldItems;

}

