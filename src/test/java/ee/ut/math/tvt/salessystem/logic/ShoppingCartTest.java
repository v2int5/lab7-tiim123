package ee.ut.math.tvt.salessystem.logic;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ee.ut.math.tvt.salessystem.dao.*;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;

import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {

	
	private InMemorySalesSystemDAO dao;
	private ShoppingCart cart;
	private SoldItem item;
	
	
	@Before
	public void before(){
		dao = new InMemorySalesSystemDAO();
		cart = new ShoppingCart(dao);
		item = new SoldItem(dao.findStockItem(1), 1);
		cart.addItem(item);
	}
	
	
	@Test
	public void testAddingNewItem(){
		cart.addItem(new SoldItem(dao.findStockItem(2), 1));
		assertEquals(cart.getAll().get(0).getQuantity(), 1.0, 0.0001);
	}
	
	@Test
	public void testAddingExistingItem() {
		cart.addItem(item);
		assertEquals(cart.getAll().get(0).getQuantity(), 2.0, 0.0001);
	}
	
	
	
	@Test (expected = Exception.class)
	public void testAddingNegativeQuantityItem(){
		cart.addItem(new SoldItem(dao.findStockItem(0), -1));
	}
	
	@Test (expected = Exception.class)
	public void testAddingItemWithQuantityTooLarge(){
		cart.addItem(new SoldItem(dao.findStockItem(0), dao.findStockItem(0).getQuantity() + 1));
	}
	
	@Test (expected = Exception.class)
	public void testAddingItemWithSumQuantityTooLarge(){
		for(int i = 0; i <= dao.findStockItem(0).getQuantity() + 1 ; i++){
			cart.addItem(new SoldItem(dao.findStockItem(0), dao.findStockItem(0).getQuantity() + 1));
		}
	}
	
	@Test
	public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity(){
		int quantityBeforeShopping = item.getStockItem().getQuantity();
		cart.submitCurrentPurchase();
		assertNotSame(quantityBeforeShopping, item.getStockItem().getQuantity());
	}
	
	@Test
	public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction(){
		dao.resetTransactions();
		cart.addItem(new SoldItem(dao.findStockItem(3), 1));
		cart.submitCurrentPurchase();
		assertEquals(dao.getTransactions().get(0), "begin");
		assertEquals(dao.getTransactions().get(1), "commit");
		
	}
	
	@Test
	public void testSubmittingCurrentOrderCreatesHistoryItem (){
		cart = new ShoppingCart(dao);
		item = new SoldItem(dao.findStockItem(3), 1);
		List<SoldItem> items = new ArrayList<>();
		items.add(item);
		cart.addItem(item);
		int NrOfHistoryItems = dao.findAllHistoryItems().size();
		cart.submitCurrentPurchase();
		assertNotSame(NrOfHistoryItems, dao.findAllHistoryItems().size());
		for(int i = 0; i < dao.findAllHistoryItems().get(dao.findAllHistoryItems().size()-1).getSoldItems().size(); i++){
			assertEquals(items.get(i), dao.findAllHistoryItems().get(dao.findAllHistoryItems().size()-1).getSoldItems().get(i));
		}
	}
	
	@Test
	public void testSubmittingCurrentOrderSavesCorrectTime(){
		cart.addItem(item);
		cart.submitCurrentPurchase();
		long secondsHistory = dao.findAllHistoryItems().get(dao.findAllHistoryItems().size()-1).getDateTime().getSecond();
		LocalDateTime time = LocalDateTime.now();
		long secondsNow = time.getSecond();
		assertEquals(secondsHistory, secondsNow, 1);
	}
	
	@Test
	public void testCancellingOrder(){
		dao.resetStockItems();
		cart.addItem(item);
		cart.cancelCurrentPurchase();
		cart.addItem(new SoldItem(dao.findStockItem(3), 1));
		cart.submitCurrentPurchase();
		assertEquals(dao.getLastHistoryItem().getSoldItems().get(0).getName(), dao.findStockItem(3).getName());
	}
	
	@Test
	public void testCancellingOrderQuanititesUnchanged(){
		int startQuantity = item.getStockItem().getQuantity();
		cart.addItem(item);
		cart.cancelCurrentPurchase();
		assertEquals(startQuantity, item.getStockItem().getQuantity(), 0.1);
	}

}
