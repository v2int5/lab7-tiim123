package ee.ut.math.tvt.salessystem.logic;


import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;


/*testAddingItemBeginsAndCommitsTransaction - check that methods beginTransaction and commitTransaction
 are both called exactly once and that order
testAddingNewItem - check that a new item is saved through the DAO
testAddingExistingItem - check that adding a new item increases the quantity and the saveStockItem method of the DAO is not called
testAddingItemWithNegativeQuantity - check that adding an item with negative quantity results in an exception*/

public class WarehouseTest {
	private InMemorySalesSystemDAO dao;
	private StockItem item;


    @Before
	public void before(){
		dao = new InMemorySalesSystemDAO();
		item = new StockItem();
	}


	@Test
    public void testAddingItemBeginsAndCommitsTransaction(){
        dao.resetTransactions();
        dao.saveStockItem(item);
        assertEquals(dao.getTransactions().get(0), "begin");
        assertEquals(dao.getTransactions().get(1), "commit");
        assertNotEquals(dao.getTransactions().get(2), "begin");
        assertNotEquals(dao.getTransactions().get(2), "commit");
    }

    @Test
    public void testAddingNewItem(){
        dao.saveStockItem(item);
        assertEquals(item, dao.findStockItem(item.getId()));
    }

    @Test
    public void testAddingExistingItem(){
        int numBefore = dao.findStockItem(item.getId()).getQuantity();
        dao.saveStockItem(item);
        assertNotEquals(numBefore, dao.findStockItem(item.getId()).getQuantity());
    }

    @Test (expected = Exception.class)
    public void testAddingItemWithNegativeQuantity(){
        dao.saveStockItem(new StockItem(new Long(9999999), "", "", 0.0, -1));
    }
}
